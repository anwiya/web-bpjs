
<section>
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <?php
        // MENGHUBUNGKAN/MEMANGGIL KE SCRIPT DB.PHP BIAR BISA MANGGIL VARIABEL $CONN YANG ADA DI DB.PHP
        include 'db.php';

        $data = mysqli_query($conn, "SELECT konten.*, section.section, halaman.halaman FROM konten JOIN halaman ON halaman.id_halaman = konten.id_halaman JOIN section ON section.id_section = konten.id_section WHERE halaman='claim' AND section='banner'");
        if (mysqli_num_rows($data) > 0) {
        while ($d = mysqli_fetch_array($data)) :
    ?>
    <img class="d-block w-100" src="<?= '../../../../../img/' . $d['image'];?>" alt="<?= $d['head_title'] ?>">
    <section class="generic text-center">

      <h2 class="text-uppercase text-bold"><?= $d['head_title'] ?></h2>
    </section>
    <?php if(!empty($d['head_title'])) : ?>
     <hr class="hr-mid">
    <?php endif; ?>
    <?php
        endwhile; ?>
    <?php } ?>
    </div>
    <?php
        // MENGHUBUNGKAN/MEMANGGIL KE SCRIPT DB.PHP BIAR BISA MANGGIL VARIABEL $CONN YANG ADA DI DB.PHP
        include 'db.php';

        $data = mysqli_query($conn, "SELECT konten.*, section.section, halaman.halaman FROM konten JOIN halaman ON halaman.id_halaman = konten.id_halaman JOIN section ON section.id_section = konten.id_section WHERE halaman='claim' AND section='card'");
        if (mysqli_num_rows($data) > 0) {
        while ($d = mysqli_fetch_array($data)) :
    ?>
    <div class="container mb-4">
        <section class="d-flex generic text-center">
          <div class="card" style="width: 20%;">
            <?php if (!empty($d['image'])) : ?>
             <img class="card-img-top img-generic img-responsive" src="<?= '../../../../../img/' . $d['image'];?>">
            <?php endif; ?>
            <div class="card-body">
              <p class="font-2 card-text"><?= $d['head_title'] ?></p>
            </div>
          </div>
          <div class="ml-4 align-middle" style="width: 80%;">
            <h3 style="float: left;"><?= $d['content_title'] ?></h3>
            <p style="margin-top: 10rem; float: left;"><?= $d['content'] ?></p>
          </div>
        </section>
    </div>
    <?php
        endwhile; ?>
    <?php } ?>
</section>
