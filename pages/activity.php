<section class="container">
    <?php
    // MENGHUBUNGKAN/MEMANGGIL KE SCRIPT DB.PHP BIAR BISA MANGGIL VARIABEL $CONN YANG ADA DI DB.PHP
    include 'db.php';

    $data = mysqli_query($conn, "SELECT konten.*, section.section, halaman.halaman FROM konten JOIN halaman ON halaman.id_halaman = konten.id_halaman JOIN section ON section.id_section = konten.id_section WHERE halaman='informasi kegiatan' AND section='informasi'");
    if (mysqli_num_rows($data) > 0) {
        while ($d = mysqli_fetch_array($data)) :
    ?>
            <div class="container">
                <section class="generic text-center">
                    <h2 class="text-uppercase text-bold"><?= $d['head_title'] ?></h2>
                    <?php if (!empty($d['head_title'])) : ?>
                        <hr class="hr-mid">
                    <?php endif; ?>
                    <?php if (!empty($d['image'])) : ?>
                        <img class="img-generic img-responsive" src="<?= '../../../../../img/' . $d['image']; ?>">
                    <?php endif; ?>
                </section>
                <section class="font-2">
                    <h3 class="text-uppercase text-bold"><?= $d['content_title'] ?></h3>
                    <p class="generic-p" style="text-align: justify;">
                        <?= $d['content'] ?>
                    </p>
                </section>
            </div>
        <?php
        endwhile; ?>
    <?php } ?>
</section>