
<section>
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <?php
        // MENGHUBUNGKAN/MEMANGGIL KE SCRIPT DB.PHP BIAR BISA MANGGIL VARIABEL $CONN YANG ADA DI DB.PHP
        include 'db.php';

        $data = mysqli_query($conn, "SELECT konten.*, section.section, halaman.halaman FROM konten JOIN halaman ON halaman.id_halaman = konten.id_halaman JOIN section ON section.id_section = konten.id_section WHERE halaman='home' AND section='banner'");
        if (mysqli_num_rows($data) > 0) {
        while ($d = mysqli_fetch_array($data)) :
    ?>
    <img class="d-block w-100" src="<?= '../../../../../img/' . $d['image'];?>" alt="<?= $d['head_title'] ?>">
    <?php
        endwhile; ?>
    <?php } ?>
    </div>
    <?php
        // MENGHUBUNGKAN/MEMANGGIL KE SCRIPT DB.PHP BIAR BISA MANGGIL VARIABEL $CONN YANG ADA DI DB.PHP
        include 'db.php';

        $data = mysqli_query($conn, "SELECT konten.*, section.section, halaman.halaman FROM konten JOIN halaman ON halaman.id_halaman = konten.id_halaman JOIN section ON section.id_section = konten.id_section WHERE halaman='home' AND section='sejarah kami'");
        if (mysqli_num_rows($data) > 0) {
        while ($d = mysqli_fetch_array($data)) :
    ?>
    <div class="container">
        <section class="generic text-center">
           <h2 class="text-uppercase text-bold"><?= $d['head_title'] ?></h2>
           <?php if(!empty($d['head_title'])) : ?>
            <hr class="hr-mid">
           <?php endif; ?>
           <?php if (!empty($d['image'])) : ?>
            <img class="img-generic img-responsive" src="<?= '../../../../../img/' . $d['image'];?>">
           <?php endif; ?>
        </section>
        <section class="font-2">
        <h3 class="text-uppercase text-bold"><?= $d['content_title'] ?></h3>
            <p class="generic-p" style="text-align: justify;"> 
                <?= $d['content'] ?>
            </p>
        </section>
    </div>
    <?php
        endwhile; ?>
    <?php } ?>
    <section class="text-center">

        <?php
        // MENGHUBUNGKAN/MEMANGGIL KE SCRIPT DB.PHP BIAR BISA MANGGIL VARIABEL $CONN YANG ADA DI DB.PHP
        include 'db.php';
        
        $data = mysqli_query($conn, "SELECT konten.*, section.section, halaman.halaman FROM konten JOIN halaman ON halaman.id_halaman = konten.id_halaman JOIN section ON section.id_section = konten.id_section WHERE halaman='home' AND section='bank list'");
        if (mysqli_num_rows($data) > 0) {
            while ($d = mysqli_fetch_array($data)) :
                ?>
            <h2 class="text-uppercase text-bold"><?= $d['head_title'] ?></h2>
            <?php if(!empty($d['head_title'])) : ?>
                <hr class="hr-mid">
                <?php endif; ?>
                <?php
               endwhile; ?>
           <?php } ?>
           <div class="container mt-5 mb-5">
        <div class="row">
                    <?php
                        // MENGHUBUNGKAN/MEMANGGIL KE SCRIPT DB.PHP BIAR BISA MANGGIL VARIABEL $CONN YANG ADA DI DB.PHP
                        include 'db.php';
                        
                        $data = mysqli_query($conn, "SELECT konten.*, section.section, halaman.halaman FROM konten JOIN halaman ON halaman.id_halaman = konten.id_halaman JOIN section ON section.id_section = konten.id_section WHERE halaman='home' AND section='bank list'");
                        if (mysqli_num_rows($data) > 0) {
                            while ($d = mysqli_fetch_array($data)) :
                                ?>
                    <div class="col d-flex justify-content-center">
                        <img style="height: 5rem;" src="<?= '../../../../../img/' . $d['image'];?>" alt="<?= $d['content_title'] ?>">
                    </div>
                    <?php
                        endwhile; ?>
                    <?php } ?>
                </div>
            </div>
        </section>
        </section>
        