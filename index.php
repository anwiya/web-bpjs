<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="assets/font-awesome/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Lora:400i" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Poppins:400,600" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Crimson+Text" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">
  <script type="text/javascript" src="../fancyapps-fancybox-18d1712/lib/jquery-1.10.1.min.js"></script>
  <script type="text/javascript" src="../fancyapps-fancybox-18d1712/source/jquery.fancybox.js?v=2.1.5"></script>

  <link rel="stylesheet" type="text/css" href="../fancyapps-fancybox-18d1712/source/jquery.fancybox.css?v=2.1.5" media="screen">
  <script type="text/javascript" src="assets/js/script.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $(".perbesar").fancybox();
    });
  </script>
  <title>BPJS KETENAGAKERJAAN</title>
</head>

<body>
  <!-- div ngebuat body jadi blur -->
  <div class="content">
    <div class="overlay" id="overlay" onclick="closeNav()"></div>


    <!-- navigasi menu -->
    <section>
      <div class="container-lg">

        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
          <a class="navbar-brand" style="margin-top: -30px;" href="index.php"><img src="https://www.bpjsketenagakerjaan.go.id/newweb/images/logo-bpjamsostek.svg" alt="Ramzi" width="150"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav float-right" style="margin-left : 2rem; margin-right: 3rem;">
              <li class="nav-item" style="margin-top: 1rem;">
                <a class="nav-link <?= $_GET['p'] == 'home' ? 'active' : '' ?>" href="index.php?p=home">Home</a>
              </li>
              <li class="nav-item" style="margin-top: 1rem;">
                <a class="nav-link <?= $_GET['p'] == 'claim' ? 'active' : '' ?>" href="index.php?p=claim">Klaim</a>
              </li>
              <li class="nav-item" style="margin-top: 1rem; margin-bottom: 1rem;">
                <a class="nav-link <?= $_GET['p'] == 'contact' ? 'active' : '' ?>" href="index.php?p=contact">Kontak kami</a>
              </li>
              <li class="nav-item" style="margin-top: 1rem; margin-bottom: 1rem;">
                <a class="nav-link <?= $_GET['p'] == 'articel' ? 'active' : '' ?>" href="index.php?p=articel">Artikel</a>
              </li>
              <li class="nav-item" style="margin-top: 1rem; margin-bottom: 1rem;">
                <a class="nav-link <?= $_GET['p'] == 'activity' ? 'active' : '' ?>" href="index.php?p=activity">Informasi Kegiatan</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <div class="content">
        <?php
        $page_dir = 'pages';
        if (!empty($_GET['p'])) {
          $page = scandir($page_dir, 0);

          unset($page[0], $page[1]);

          $p = $_GET['p'];
          if (in_array($p . '.php', $page)) {
            include($page_dir . '/' . $p . '.php');
          } else {
            echo "Halaman tidak ditemukan!";
          }
        } else {
          include($page_dir . '/home.php');
        }


        ?>
      </div>
      <section class="footer bg-primary">
        <div class="container">
          <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
              <div class="footer-p1">
                <p class="font-2">Copy right &copy 2022 by ramskuy</p>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
              <img class="img-responsive foot-img" src="https://www.bpjsketenagakerjaan.go.id/newweb/images/logo-bpjamsostek.svg">
            </div>
          </div>
        </div>
      </section>
  </div>

</body>

</html>